
const routes = [
  { path: '/', component: () => import('layouts/Home.vue') },
  { path: '/pages/Login.vue', component: () => import('pages/Login.vue') },
  { path: '/pages/Cadastro.vue', component: () => import('pages/Cadastro.vue') },
  {
    path: '/layouts/Sistema.vue',
    component: () => import('layouts/Sistema.vue'),
    children: [
      { path: '/pages/Profile.vue', component: () => import('pages/Profile.vue') },
      { path: '/pages/DashBoard.vue', component: () => import('pages/DashBoard.vue') },
      { path: '/pages/VideoDetail.vue', component: () => import('pages/VideoDetail.vue') },
      { path: '/pages/VideoGallery.vue', component: () => import('pages/VideoGallery.vue') },
      { path: '/pages/Following.vue', component: () => import('pages/Following.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
